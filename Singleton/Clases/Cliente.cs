﻿using System;
namespace Singleton.Clases
{
    public class Cliente
    {
        public Cliente(string Nombre)
        {
            this.Nombre = Nombre;
        }
        public string Nombre { get; set; }
    }
}
