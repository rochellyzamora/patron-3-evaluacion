﻿using System;
using Singleton.Clases;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {

            Singletone.Instancia.Cliente = new Cliente("edgardopanchana");

            //Que línea de código hace falta para mostrar por pantalla el nombre del cliente de la instacia sigleton?                                    Console.ReadKey();
        }
    }
}
